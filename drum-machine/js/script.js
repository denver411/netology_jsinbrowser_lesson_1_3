'use strict'

let drums = document.getElementsByClassName('drum-kit__drum');

for (let drum of drums) {
  drum.onclick = () => {
    let drumButton = drum.getElementsByTagName('audio')[0];
    drumButton.pause();
    drumButton.currentTime = 0;
    drumButton.play();
  }
}