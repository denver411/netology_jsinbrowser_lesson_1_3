'use strict'

let player = document.getElementsByClassName('mediaplayer')[0];
let currentSong = player.getElementsByTagName('audio')[0];
let songTitle = document.getElementsByClassName('title')[0];
const songs = ['LA Chill Tour', 'LA Fusion Jam', 'This is it band'];
let songIndex = 0;
currentSong.src = `mp3/${songs[songIndex]}.mp3`;
songTitle.title = songs[songIndex];

let playButton = document.getElementsByClassName('playstate')[0];
let stopButton = document.getElementsByClassName('stop')[0];
let prevButton = document.getElementsByClassName('back')[0];
let nextButton = document.getElementsByClassName('next')[0];

playButton.onclick = () => {
  if (currentSong.paused) {
    currentSong.play();
    player.classList.add('play');
  } else {
    currentSong.pause();
    player.classList.remove('play');
  }
}

stopButton.onclick = () => {
  currentSong.pause();
  currentSong.currentTime = 0;
  player.classList.remove('play');
}

const changeSong = function (currentSong, songIndex) {
  if (!currentSong.paused) {
    currentSong.src = `mp3/${songs[songIndex]}.mp3`;
    songTitle.title = songs[songIndex];
    currentSong.play();
  } else {
    currentSong.src = `mp3/${songs[songIndex]}.mp3`;
    songTitle.title = songs[songIndex];
  }
}

prevButton.onclick = () => {
  songIndex--;
  if (songIndex < 0) {
    songIndex = songs.length - 1;
  }
  changeSong(currentSong, songIndex);
}

nextButton.onclick = () => {
  songIndex++;
  if (songIndex >= songs.length) {
    songIndex = 0;
  }
  changeSong(currentSong, songIndex);
}